from keboola import docker
import requests
import base64
import json
import csv

# initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

# access the supplied values
COMMUNITY = cfg.get_parameters()['COMMUNITY']
#RESOURCE = cfg.get_parameters()['RESOURCE']
USERNAME = cfg.get_parameters()['USERNAME']
PASSWORD = cfg.get_parameters()['PASSWORD']


def csv_to_json(file='/data/in/tables/data.csv'):
    """convert passed csv to json"""
    
    f = open(file,'rU')
    #reader = csv.DictReader(f, fieldnames = ( "id","data"))
    reader = csv.DictReader(f, fieldnames = ( "name","type","dataType"))
    
    outR = json.dumps( [ row for row in reader ] , indent=4)
    out = json.loads(str(outR))
    print "data converted to json"
    
    return out


def post_data(auth_code, data):
    """Create new variables"""
    
    URL = str("https://api.visioncritical.com/v1/applications/"+COMMUNITY+"/memberVariables")
    print URL

    HEADERS = {
        "authorization": auth_code
        ,"cache-control": "no-cache"
        #,"X-WebApi-Return-Resource":"true"
        }
    
    data = data[1:] #header removal

    for a in data:
        PAYLOAD = a
        print "Adding: "+str(a[u'name']) # left u' just for the case
        
        # Call itself
        print "sending request... response:"
        response = requests.post(URL, headers = HEADERS, data = json.dumps(PAYLOAD))
        #response = requests.request("POST", URL, json = [{"dataType": "text", "type": "open", "name": "testvariable"}], headers = HEADERS)
        print response.text
        #print response.headers
    
    print "done."


## AUTH PART
encoded = base64.b64encode(str(USERNAME + ":" + PASSWORD))
auth_code = str("Basic " + encoded)

## DATA PREP
#data_file = open('/data/in/tables/data.csv', 'w') #KBC
jsonU = csv_to_json()

## EXECUTE
post_data(auth_code, jsonU)
